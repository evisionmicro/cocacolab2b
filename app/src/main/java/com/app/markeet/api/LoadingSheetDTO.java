package com.app.markeet.api;

import java.util.List;

public class LoadingSheetDTO {
    private String Name;
    private String Date;
    private List<LoadingSheetLineDTO> Lines;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public List<LoadingSheetLineDTO> getLines() {
        return Lines;
    }

    public void setLines(List<LoadingSheetLineDTO> lines) {
        Lines = lines;
    }
}


