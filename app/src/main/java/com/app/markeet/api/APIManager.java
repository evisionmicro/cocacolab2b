package com.app.markeet.api;

import java.util.List;
import com.app.markeet.data.AppConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIManager {

    private static TestUserAPI testUserAPI = null;
    public static TestUserAPI getUserAPI()
    {
        if (testUserAPI != null)
            return testUserAPI;

        Gson gson = new GsonBuilder()
                //.setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        testUserAPI = retrofit.create(TestUserAPI.class);
        return testUserAPI;
    }

    private static PdaServiceAPI pdaServiceAPI = null;
    public static PdaServiceAPI getPdaServiceAPI(){
        if (pdaServiceAPI != null)
            return pdaServiceAPI;

        Gson gson = new GsonBuilder()
                //.setLenient()
                .create();

        final String url = AppConfig.protocol + AppConfig.ipAddress + ":" + AppConfig.port + "/PdaService/";

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        pdaServiceAPI = retrofit.create(PdaServiceAPI.class);
        return pdaServiceAPI;
    }
}
