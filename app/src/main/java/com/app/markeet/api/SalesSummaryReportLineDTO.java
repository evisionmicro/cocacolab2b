package com.app.markeet.api;

public class SalesSummaryReportLineDTO
{
    private String SerialNo;

    private String CustomerCode;

    private String CustomerName;

    private double GrossSale;

    private double Discount;

    private double FreeValue;

    private double Return;

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String serialNo) {
        SerialNo = serialNo;
    }

    public String getCustomerCode() {
        return CustomerCode;
    }

    public void setCustomerCode(String customerCode) {
        CustomerCode = customerCode;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public double getGrossSale() {
        return GrossSale;
    }

    public void setGrossSale(double grossSale) {
        GrossSale = grossSale;
    }

    public double getDiscount() {
        return Discount;
    }

    public void setDiscount(double discount) {
        Discount = discount;
    }

    public double getFreeValue() {
        return FreeValue;
    }

    public void setFreeValue(double freeValue) {
        FreeValue = freeValue;
    }

    public double getReturn() {
        return Return;
    }

    public void setReturn(double aReturn) {
        Return = aReturn;
    }
}
