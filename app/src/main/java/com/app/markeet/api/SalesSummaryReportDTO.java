package com.app.markeet.api;

import java.util.List;

public class SalesSummaryReportDTO {
    private String Name;
    private String Date;
    private List<SalesSummaryReportLineDTO> Lines;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public List<SalesSummaryReportLineDTO> getLines() {
        return Lines;
    }

    public void setLines(List<SalesSummaryReportLineDTO> lines) {
        Lines = lines;
    }
}

