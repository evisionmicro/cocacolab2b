package com.app.markeet.api;

import com.app.markeet.connection.callbacks.CallbackOrder;
import com.app.markeet.model.ActionResultDTO;
import com.app.markeet.model.Checkout;
import com.app.markeet.model.InvoiceDTO;
import com.app.markeet.model.UploadListDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface PdaServiceAPI {
    @GET("GetLoadingSheet")
    Call<LoadingSheetDTO> getLoadingSheet(@Query("imei") String imei, @Query("date") String date);

    @GET("GetRepSalesSummaryReport")
    Call<SalesSummaryReportDTO> getRepSalesSummaryReport(@Query("imei") String imei, @Query("date") String date);

    @POST("UploadBulkB2BInvoice")
    Call<ActionResultDTO> UploadBulkInvoice(
            @Body UploadListDTO checkout
    );

    @POST("GetLastUploadedInvoiceId")
    Call<InvoiceDTO> GetLastUploadedInvoiceId(
            @Query("imei") String imei, @Query("version") String version
    );
}
