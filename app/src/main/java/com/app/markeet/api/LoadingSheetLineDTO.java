package com.app.markeet.api;

public class LoadingSheetLineDTO
{
    private String Code;
    private String Name;
    private int Qty;
    private int FreeQty;

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int qty) {
        Qty = qty;
    }

    public int getFreeQty() {
        return FreeQty;
    }

    public void setFreeQty(int freeQty) {
        FreeQty = freeQty;
    }

    public int getTotalQty() {
        return Qty + FreeQty;
    }
}
