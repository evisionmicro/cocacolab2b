package com.app.markeet.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TestUserAPI {
    @GET("/users")
    Call<List<TestUser>> getUsers();
}
