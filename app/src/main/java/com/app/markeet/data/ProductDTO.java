package com.app.markeet.data;

import java.io.Serializable;
import java.util.Collection;

public class ProductDTO implements Serializable {

    public int Id;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String Description;
    
    public String Code;
    
    public String Name;
    
    String Category;
    
    String Brand;
    
    String Model;
    
    boolean TrackSerial;
    
    double SellPrice;
    
    double RetailPrice;
    
    double MRP;
    
    public double Qty;
    
    boolean IsFocused;
    
    boolean IsVatApplicable;
    
    boolean IsCompetitorProduct;
    
    boolean TrackBatch;
    
    public boolean AllowedReturn;
    
    boolean AllowedSalesOrder;
    
    int DiscountCategoryId;
    
    String ImagePath;
    
    String CategoryImagePath;
    
    double DefaultDiscountPct;
    
    int FreeIssueCategoryId;
    
    int CategoryId;
    
    int EmptyProductId;
    
    int CrateProductId;
    
    double PackSize;
    
    String Bracode;
    
    String ProductType;
    
    public boolean IsBundle;
    
    boolean AllowCases;
    
    boolean AllowDecimal;
    
    String UOM;

    public double Price;

    public double getMRP() {
        return MRP;
    }

    public void setMRP(double MRP) {
        this.MRP = MRP;
    }

//    //ForeignCollectionField(eager = true)
//    Collection<ProductSerialDTO> DistributorStockSerials;
////    ForeignCollectionField(eager = true)
//    Collection<ProductSerialDTO> PdaStockSerials;
////    oreignCollectionField(eager = true)
//    Collection<ProductLine> ProductLines;
//
//    Collection<ProductBatchesDTO> PdaStockBatches;


    public int ProductId;
    String ProductBatchSerial;
    double ProductBatchQty;
    int ProductBatchSizeId;
    int ProductBatchStyleId;
    public double BufferStockQty;
    public double TackingStockQty;
    boolean IsTaken;

    int packs = 0;
    int pieces = 0;

    public int getPacks() {
        return packs;
    }

    public void setPacks(int packs) {
        this.packs = packs;
    }

    public int getPieces() {
        return pieces;
    }

    public void setPieces(int pieces) {
        this.pieces = pieces;
    }

    public boolean isAllowedReturn() {
        return AllowedReturn;
    }

    public void setAllowedReturn(boolean allowedReturn) {
        AllowedReturn = allowedReturn;
    }

    public boolean isAllowedSalesOrder() {
        return AllowedSalesOrder;
    }

    public void setAllowedSalesOrder(boolean allowedSalesOrder) {
        AllowedSalesOrder = allowedSalesOrder;
    }

    public boolean isTaken() {
        return IsTaken;
    }

    public void setTaken(boolean taken) {
        IsTaken = taken;
    }

    public double getPackSize() {
        return PackSize;
    }

    public void setPackSize(double packSize) {
        PackSize = packSize;
    }

    public int getEmptyProductId() {
        return EmptyProductId;
    }

    public void setEmptyProductId(int emptyProductId) {
        EmptyProductId = emptyProductId;
    }

    public int getCrateProductId() {
        return CrateProductId;
    }

    public void setCrateProductId(int crateProductId) {
        CrateProductId = crateProductId;
    }

    public String getBracode() {
        return Bracode;
    }

    public void setBracode(String bracode) {
        Bracode = bracode;
    }

    public boolean getIsVatApplicable() {
        return IsVatApplicable;
    }

    public String getProductType() {
        return ProductType;
    }

    public void setProductType(String productType) {
        ProductType = productType;
    }

    public double getBufferStockQty() {
        return BufferStockQty;
    }

    public void setBufferStockQty(double bufferStockQty) {
        BufferStockQty = bufferStockQty;
    }

    public double getTackingStockQty() {
        return TackingStockQty;
    }

    public void setTackingStockQty(double tackingStockQty) {
        TackingStockQty = tackingStockQty;
    }

    public int getProductBatchSizeId() {
        return ProductBatchSizeId;
    }

    public void setProductBatchSizeId(int productBatchSizeId) {
        ProductBatchSizeId = productBatchSizeId;
    }

    public boolean isAllowCases() {
        return AllowCases;
    }

    public void setAllowCases(boolean allowCases) {
        AllowCases = allowCases;
    }

    public boolean isAllowDecimal() {
        return AllowDecimal;
    }

    public void setAllowDecimal(boolean allowDecimal) {
        AllowDecimal = allowDecimal;
    }

    public String getUOM() {
        return UOM;
    }

    public void setUOM(String UOM) {
        this.UOM = UOM;
    }

    public int getProductBatchStyleId() {
        return ProductBatchStyleId;
    }

    public void setProductBatchStyleId(int productBatchStyleId) {
        ProductBatchStyleId = productBatchStyleId;
    }

    public int getProductId() {
        return ProductId;
    }

    public void setProductId(int productId) {
        ProductId = productId;
    }

    public String getProductBatchSerial() {
        return ProductBatchSerial;
    }

    public void setProductBatchSerial(String productBatchSerial) {
        ProductBatchSerial = productBatchSerial;
    }

    public double getProductBatchQty() {
        return ProductBatchQty;
    }

    public void setProductBatchQty(double productBatchQty) {
        ProductBatchQty = productBatchQty;
    }

    public boolean isFocused() {
        return IsFocused;
    }

    public void setFocused(boolean focused) {
        IsFocused = focused;
    }

    public boolean isBundle() {
        return IsBundle;
    }

    public void setBundle(boolean bundle) {
        IsBundle = bundle;
    }

//    public Collection<ProductLine> getProductLines() {
//        return ProductLines;
//    }

//    public void setProductLines(Collection<ProductLine> productLines) {
//        ProductLines = productLines;
//    }

    public ProductDTO() {

    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public boolean isTrackSerial() {
        return TrackSerial;
    }

    public void setTrackSerial(boolean trackSerial) {
        TrackSerial = trackSerial;
    }

    public double getSellPrice() {
        return SellPrice;
    }

    public void setSellPrice(double sellPrice) {
        SellPrice = sellPrice;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    public String getCategoryImagePath() {
        return CategoryImagePath;
    }

    public void setCategoryImagePath(String categoryImagePath) {
        CategoryImagePath = categoryImagePath;
    }

//    public Collection<ProductSerialDTO> getDistributorStockSerials() {
//        return DistributorStockSerials;
//    }

    public boolean isTrackBatch() {
        return TrackBatch;
    }

    public void setTrackBatch(boolean trackBatch) {
        TrackBatch = trackBatch;
    }

//    public void setDistributorStockSerials(Collection<ProductSerialDTO> distributorStockSerials) {
//        DistributorStockSerials = distributorStockSerials;
//    }
//
//    public Collection<ProductSerialDTO> getPdaStockSerials() {
//        return PdaStockSerials;
//    }
//
//    public void setPdaStockSerials(Collection<ProductSerialDTO> pdaStockSerials) {
//        PdaStockSerials = pdaStockSerials;
//    }

    public int getDiscountCategoryId() {
        return DiscountCategoryId;
    }

    public void setDiscountCategoryId(int discountCategoryId) {
        DiscountCategoryId = discountCategoryId;
    }

//    public Collection<ProductBatchesDTO> getPdaStockBatches() {
//        return PdaStockBatches;
//    }
    public double getQty() {
        return Qty;
    }

    public void setQty(double qty) {
        Qty = qty;
    }

    public double getRetailPrice() {
        return RetailPrice;
    }

    public void setRetailPrice(double retailPrice) {
        RetailPrice = retailPrice;
    }

    public double getDefaultDiscountPct() {
        return DefaultDiscountPct;
    }

    public void setDefaultDiscountPct(double defaultDiscountPct) {
        DefaultDiscountPct = defaultDiscountPct;
    }

    public boolean isVatApplicable() {
        return IsVatApplicable;
    }

    public void setVatApplicable(boolean vatApplicable) {
        IsVatApplicable = vatApplicable;
    }

    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int categoryId) {
        CategoryId = categoryId;
    }

    public int getFreeIssueCategoryId() {
        return FreeIssueCategoryId;
    }

    public void setFreeIssueCategoryId(int freeIssueCategoryId) {
        FreeIssueCategoryId = freeIssueCategoryId;
    }

    public boolean isCompetitorProduct() {
        return IsCompetitorProduct;
    }

    public void setCompetitorProduct(boolean competitorProduct) {
        IsCompetitorProduct = competitorProduct;
    }
}
