package com.app.markeet.data;

import java.util.Locale;

public class AppConfig {

    // if you not use ads you can set this to false
    public static final boolean ENABLE_GDPR = true;

    // flag for display ads
    public static final boolean ADS_MAIN_INTERSTITIAL = true;
    public static final int ADS_MAIN_INTERSTITIAL_INTERVAL = 180; // in second
    public static final boolean ADS_NEWS_INFO_DETAILS = true;
    public static final boolean ADS_PRODUCT_DETAILS = true;
    public static String deviceId;
    public static String protocol = "http://";
//    public static String ipAddress = "138.201.29.139";//Sammana
////    public static String ipAddress = "176.9.136.71";//Sammana
//    public static String port = "30010"; //Sammana
    // tinting category icon
    public static final boolean TINT_CATEGORY_ICON = true;
    public static String ipAddress = "95.217.57.146";//Sammana
    //  public static String ipAddress = "176.9.136.71";
    public static String port = "8875";

    /* Locale.US        -> 2,365.12
     * Locale.GERMANY   -> 2.365,12
     */
    public static final Locale PRICE_LOCAL_FORMAT = Locale.US;

    /* true     -> 2.365,12
     * false    -> 2.365
     */
    public static final boolean PRICE_WITH_DECIMAL = true;

    /* true     -> 2.365,12 USD
     * false    -> USD 2.365
     */
    public static final boolean PRICE_CURRENCY_IN_END = true;

}
