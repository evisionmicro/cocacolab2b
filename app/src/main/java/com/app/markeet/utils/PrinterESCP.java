package com.app.markeet.utils;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Prathap on 8/30/2016.
 */
public class PrinterESCP implements IPrinter {

    public BluetoothAdapter mBluetoothAdapter;
    public static BluetoothSocket mmSocket;
    static BluetoothDevice mmDevice;

    public OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;

    byte[] readBuffer;
    int readBufferPosition;
    int pageHeight = 40;//64
    int counter;
    volatile boolean stopWorker;

    public PrinterESCP(int height) {
        pageHeight = height;
        findBT();
        openBT();
        //openBTWithRetry();
    }

    public void Init() {
        findBT();
        openBT();
        //openBTWithRetry();

    }
    public void CloseConnection() throws IOException {
        mmSocket.close();
    }
    private void openBTWithRetry()
    {
        boolean status = openBT();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            int count = 0;
            while (status && count < 10) {
                status = openBT();
                count++;

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public int findBT() {

        int res = 0;
        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (mBluetoothAdapter == null) {

                Log.d("adapter", "No bluetooth adapter available");
                res = 0;
            }

            if (!mBluetoothAdapter.isEnabled()) {
                res = 1;
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter
                    .getBondedDevices();
            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {

                    String name = device.getName();
                    if (device.getName().equals(Globles.bluetoothDevicesName)) {
                        mmDevice = device;
                        break;
                    }
                }
            }

            Log.d("device", "Device found");
        } catch (NullPointerException e) {
            e.printStackTrace();
            res = 0;
        } catch (Exception e) {
            e.printStackTrace();
            res = 0;
        }
        return res;
    }

    public Boolean openBT() {
        Boolean res = true;
        try {
            // Standard SerialPortService ID
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            mmSocket.connect();
            mmOutputStream = mmSocket.getOutputStream();
            mmInputStream = mmSocket.getInputStream();

            beginListenForData();

            Log.d("Bluetooth", "Opened");
        } catch (NullPointerException e) {
            e.printStackTrace();
            res = false;
        } catch (Exception e) {
            e.printStackTrace();
            res = false;
        }
        return res;
    }

    public void beginListenForData() {
        try {
            final Handler handler = new Handler();

            // This is the ASCII code for a newline character
            final byte delimiter = 10;

            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];

            workerThread = new Thread(new Runnable() {
                public void run() {
                    while (!Thread.currentThread().isInterrupted()
                            && !stopWorker) {

                        try {

                            int bytesAvailable = mmInputStream.available();
                            if (bytesAvailable > 0) {
                                byte[] packetBytes = new byte[bytesAvailable];
                                mmInputStream.read(packetBytes);
                                for (int i = 0; i < bytesAvailable; i++) {
                                    byte b = packetBytes[i];
                                    if (b == delimiter) {
                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.length);
                                        final String data = new String(
                                                encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;

                                        handler.post(new Runnable() {
                                            public void run() {
                                                // myLabel.setText(data);
                                                Log.d("data", data.toString());
                                            }
                                        });
                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }

                        } catch (IOException ex) {
                            stopWorker = true;
                        }

                    }
                }
            });

            workerThread.start();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void format(String text) {
        String msg = text;
        try {

            mmOutputStream.write(msg.getBytes());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                mmOutputStream.flush();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    @Override
    public void printLine(String text) {
        String msg = text + "\n";
        try {
            mmOutputStream.write(msg.getBytes());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                mmOutputStream.flush();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        counter++;

        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void AddLineSpace() {
        String msg = "\n";
        try {

            mmOutputStream.write(msg.getBytes());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                mmOutputStream.flush();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        counter++;

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void AddLineSpace18() {
        format((char) 27 + "n");
    }

    public void endPrint() {
        int balance = pageHeight - (counter % pageHeight);
        for (int i = 0; i < balance; i++)
            printLine("");
        counter = 0;
    }

    @Override
    public void alignLeft() {
        format((char) 27 + "a0");
    }

    @Override
    public void alignRight() {
        format((char) 27 + "a2");
    }

    @Override
    public void alignCenter() {
        format("" + (char) 27 + (char) 97 + (char) 2);
    }

    @Override
    public void formFeed() {
        format("" + (char) 12);
    }

    @Override
    public void feedLine(int lines) {
        format((char) 27 + "d" + (char) lines);
    }

    @Override
    public void bold() {
        format("" + (char) 27 + (char) 69);
    }

    @Override
    public void doubleHeight() {
        format("" + (char) 27 + "w");
    }

    @Override
    public void doubleWidth() {
        format((char) 27 + "" + (char) 87 + (char) 1);
    }

    @Override
    public void doubleHeightAndWidth() {
        format((char) 27 + "!" + (char) 48);
    }

    @Override
    public void cancelDoubleHeightAndWidth() {
        format((char) 27 + "!" + (char) 0);
    }

    @Override
    public void setFont(int n) {
        format((char) 27 + "M" + (char) n);
    }

    @Override
    public void bitImage(int nl, int nh, byte[] strip) {
        format((char) 27 + "*" + (char) 1 + (char) nl + (char) nh + strip);
    }

    @Override
    public void reset() {
        format((char) 27 + "@");
    }

//    public void testInvoicePrint() throws IOException {
//        try {
//            DecimalFormat decformat = new DecimalFormat("#,##0.00");
//            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
//            Date date = new Date();
//
//            doubleWidth();
//            bold();
//            printLine(StringUtils.center("eVision Microsystems", 22));
//            reset();
//
//            printLine(StringUtils.center("No 33/2, Horana Rd, Alubomulla", 45));
//            printLine(StringUtils.center("Tel : 0382232433, Fax : 0382246567", 45));
//            printLine(StringUtils.center("www.evision.lk", 45));
//            reset();
//
//            bold();
//            doubleWidth();
//            printLine(StringUtils.center("Invoice", 22));
//            reset();
//
//            printLine(String.format("%-22s%22s", "Inv No : " + "INV09890", "Date : " + dateFormat.format(date)));
//            printLine(String.format("%-25s", "Terms : ", ""));
//            printLine("");
//
//            printLine("Customer :");
//            printLine("Saman perera");
//            printLine("No 123,Gunasekara Mawatha,Walana,Panadura");
//            printLine("Tel : 0382234876");
//            printLine("");
//
//            bold();
//            printLine(StringUtils.drawLine(44));
//            printLine("   Item             Qty       Rate    Amount");
//            printLine(StringUtils.drawLine(44));
//            reset();
//            printLine("01 MD mixed friut jam");
//            printLine(String.format("%3s%11s%9s%11s%10s", "", "", "100", "125.00", "12,500.00"));
//            bold();
//            printLine(StringUtils.drawLine(44));
//            reset();
//            printLine(String.format("%30s%14s", "Sub Total ", "12,500.00"));
//            printLine(String.format("%30s%14s", "Less Discount ", "0.00"));
//
//            bold();
//            printLine(String.format("%30s%14s", "", "--------------"));
//            printLine(String.format("%30s%14s", "Net Total ", "12,500.00"));
//            printLine(String.format("%30s%14s", "", "---------------"));
//            reset();
//
//            printLine("");
//            printLine("");
//            printLine("");
//
//            printLine(String.format("%-22s%22s", "------------------", "-------------"));
//            printLine(String.format("%-22s%22s", "Customer Signature", "Authorized By"));
//
//        } catch (Exception e) {
//            //Toast.makeText(CONTEXT, "Printing Error", Toast.LENGTH_SHORT).show();
//        }
//
//    }
}
