package com.app.markeet.utils;

import java.io.IOException;

/**
 * Created by Prathap on 8/30/2016.
 */
public interface IPrinter {

    void printLine(String text);
    void alignLeft();
    void alignRight();
    void alignCenter();
    void formFeed();
    void feedLine(int lines);
    void bold();
    void doubleHeight();
    void doubleWidth();
    void doubleHeightAndWidth();
    void cancelDoubleHeightAndWidth();
    void setFont(int n);
    void bitImage(int nl, int nh, byte[] strip);
    void reset();
    void AddLineSpace();
    void AddLineSpace18();
    void Init();
    void CloseConnection() throws IOException;


}