package com.app.markeet.model;

import com.app.markeet.data.ProductDTO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Product implements Serializable {

    public Long id;
    public String name;
    public String image;
    public Double price;
    public Double price_discount;
    public Long stock;
    public Integer draft;
    public String Description;
    public String Code;
    public String Brang;

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getBrang() {
        return Brang;
    }

    public void setBrang(String brang) {
        Brang = brang;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String Category;

    public String Description() {
        return Description;
    }

    public void Description(String Description) {
        this.Description = Description;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPrice_discount() {
        return price_discount;
    }

    public void setPrice_discount(Double price_discount) {
        this.price_discount = price_discount;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    public Integer getDraft() {
        return draft;
    }

    public void setDraft(Integer draft) {
        this.draft = draft;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Long created_at) {
        this.created_at = created_at;
    }

    public Long getLast_update() {
        return last_update;
    }

    public void setLast_update(Long last_update) {
        this.last_update = last_update;
    }

    public String description;
    public String status;
    public Long created_at;
    public Long last_update;
    public static List<ProductDTO> p = new ArrayList<>();
    public List<Category> categories = new ArrayList<>();
    public List<ProductImage> product_images = new ArrayList<>();

    public List<Product> getProductsForCoke() {

        List<Product> list = new ArrayList<>();
        Product pro = new Product();
        pro.id = Long.valueOf(5);
        pro.status = "READY STOCK";
        pro.image = "Bosch-Brake-Pads.jpg";
        pro.price = 150.00;
        pro.name = "CR Book 120";
        pro.categories = new ArrayList<>();
        pro.stock = Long.valueOf(150);
        pro.product_images = new ArrayList<>();
        pro.price_discount = 0.00;
        pro.last_update = Long.valueOf(1000000);
//        pro.description = "<ul class=\"a-unordered-list a-vertical a-spacing-none\" style=\"box-sizing: border-box; margin: 0px 0px 0px 18px; color: rgb(148, 148, 148); padding: 0px; font-family: &quot;Amazon Ember&quot;, Arial, sans-serif; font-size: 13px;\"><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Care Instructions: First Wash Dry Clean Only. Do Not Spin In Order To Prevent Garment From Becoming Mishappen.</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Fabric : Crepe</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Chinese Collar 3/4Th Sleeve Solid Straight Kurta</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Available Size:S,M,L,XL,XXL,XXXL</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Straight Kurta</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Please check ZIYAA size chart for the garments measurements and order a garment size that is at least 2\" greater than your body measurement for the ease of putting on and taking off the garment.</span></li></ul>";
        pro.description = "CR Book of 120 pages";

        Product pro2 = new Product();
        pro2.id = Long.valueOf(6);
        pro2.name = "CR Book 200";
        pro2.status = "READY STOCK";
        pro2.image = "B.jpg";
        pro2.price = 200.00;
        pro2.categories = new ArrayList<>();
        pro2.stock = Long.valueOf(150);
        pro2.product_images = new ArrayList<>();
        pro2.price_discount = 0.00;
//        pro2.description = "<ul class=\"a-unordered-list a-vertical a-spacing-none\" style=\"box-sizing: border-box; margin: 0px 0px 0px 18px; color: rgb(148, 148, 148); padding: 0px; font-family: &quot;Amazon Ember&quot;, Arial, sans-serif; font-size: 13px;\"><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Care Instructions: First Wash Dry Clean Only. Do Not Spin In Order To Prevent Garment From Becoming Mishappen.</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Fabric : Crepe</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Chinese Collar 3/4Th Sleeve Solid Straight Kurta</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Available Size:S,M,L,XL,XXL,XXXL</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Straight Kurta</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Please check ZIYAA size chart for the garments measurements and order a garment size that is at least 2\" greater than your body measurement for the ease of putting on and taking off the garment.</span></li></ul>";
        pro2.description = "CR Book of 200 pages";

        list.add(pro);
        list.add(pro2);
        return list;
    }

    public List<Product> getProductsForFanta() {

        List<Product> list = new ArrayList<>();
        Product pro = new Product();
        pro.id = Long.valueOf(7);
        pro.status = "READY STOCK";
        pro.image = "B.png";
        pro.price = 25.00;
        pro.name = "Buttergel";
        pro.categories = new ArrayList<>();
        pro.stock = Long.valueOf(150);
        pro.product_images = new ArrayList<>();
        pro.price_discount = 0.00;
//        pro.description = "<ul class=\"a-unordered-list a-vertical a-spacing-none\" style=\"box-sizing: border-box; margin: 0px 0px 0px 18px; color: rgb(148, 148, 148); padding: 0px; font-family: &quot;Amazon Ember&quot;, Arial, sans-serif; font-size: 13px;\"><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Care Instructions: First Wash Dry Clean Only. Do Not Spin In Order To Prevent Garment From Becoming Mishappen.</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Fabric : Crepe</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Chinese Collar 3/4Th Sleeve Solid Straight Kurta</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Available Size:S,M,L,XL,XXL,XXXL</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Straight Kurta</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Please check ZIYAA size chart for the garments measurements and order a garment size that is at least 2\" greater than your body measurement for the ease of putting on and taking off the garment.</span></li></ul>";
        pro.description = "smooth writing";

        Product pro2 = new Product();
        pro2.id = Long.valueOf(8);
        pro2.name = "Atlas chooty";
        pro2.status = "READY STOCK";
        pro2.image = "A.jpg";
        pro2.price = 10.00;
        pro2.categories = new ArrayList<>();
        pro2.stock = Long.valueOf(150);
        pro2.product_images = new ArrayList<>();
        pro2.price_discount = 0.00;
//        pro2.description = "<ul class=\"a-unordered-list a-vertical a-spacing-none\" style=\"box-sizing: border-box; margin: 0px 0px 0px 18px; color: rgb(148, 148, 148); padding: 0px; font-family: &quot;Amazon Ember&quot;, Arial, sans-serif; font-size: 13px;\"><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Care Instructions: First Wash Dry Clean Only. Do Not Spin In Order To Prevent Garment From Becoming Mishappen.</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Fabric : Crepe</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Chinese Collar 3/4Th Sleeve Solid Straight Kurta</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Available Size:S,M,L,XL,XXL,XXXL</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Straight Kurta</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Please check ZIYAA size chart for the garments measurements and order a garment size that is at least 2\" greater than your body measurement for the ease of putting on and taking off the garment.</span></li></ul>";
        pro2.description = "fine thin writing";

        list.add(pro);
        list.add(pro2);
        return list;
    }

//    public List<Product> getProducts(long categoryId){
//
//        List<Product> list = new ArrayList<>();
//        switch ((int)categoryId){
//            case 1 : list = getProductsForCoke();
//                break;
//            case 2 : list = getProductsForFanta();
//                break;
//            case 3 : list = getProductsForFanta();
//                break;
//            default :  list = new ArrayList<>();
//                break;
//        }
//        return list;
//    }

    //    public Product getProduct(long id){
//
//        Product pro = new Product();
//        switch ((int)id){
//            case 5 :
//                pro.id = Long.valueOf(1);
//                pro.status = "READY STOCK";
//                pro.image = "x.jpg";
//                pro.price = 150.00;
//                pro.name = "CR-Book 120 pages";
//                pro.categories = new ArrayList<>();
//                pro.stock = Long.valueOf(150);
//                pro.product_images = new ArrayList<>();
//                pro.price_discount = 0.00;
//                pro.description = "Atlas CR book with 120 pages";
////                pro.description = "<ul class=\"a-unordered-list a-vertical a-spacing-none\" style=\"box-sizing: border-box; margin: 0px 0px 0px 18px; color: rgb(148, 148, 148); padding: 0px; font-family: &quot;Amazon Ember&quot;, Arial, sans-serif; font-size: 13px;\"><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Care Instructions: First Wash Dry Clean Only. Do Not Spin In Order To Prevent Garment From Becoming Mishappen.</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Fabric : Crepe</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Chinese Collar 3/4Th Sleeve Solid Straight Kurta</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Available Size:S,M,L,XL,XXL,XXXL</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Straight Kurta</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Please check ZIYAA size chart for the garments measurements and order a garment size that is at least 2\" greater than your body measurement for the ease of putting on and taking off the garment.</span></li></ul>";
//                pro.last_update = Long.valueOf(100000000);
//                break;
//            case 6 :
//                pro.id = Long.valueOf(2);
//                pro.status = "READY STOCK";
//
//                pro.image = "A.png";
//
//                pro.image = "tataxy-ace-front-disc-brake-pads-1312687.jpg";
//
//                pro.price = 250.00;
//                pro.name = "CR-Book 200 pages";
//                pro.categories = new ArrayList<>();
//                pro.stock = Long.valueOf(150);
//                pro.product_images = new ArrayList<>();
//                pro.price_discount = 0.00;
//                pro.description = "Atlas CR book with 200 pages";
////                pro.description = "<ul class=\"a-unordered-list a-vertical a-spacing-none\" style=\"box-sizing: border-box; margin: 0px 0px 0px 18px; color: rgb(148, 148, 148); padding: 0px; font-family: &quot;Amazon Ember&quot;, Arial, sans-serif; font-size: 13px;\"><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Care Instructions: First Wash Dry Clean Only. Do Not Spin In Order To Prevent Garment From Becoming Mishappen.</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Fabric : Crepe</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Chinese Collar 3/4Th Sleeve Solid Straight Kurta</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Available Size:S,M,L,XL,XXL,XXXL</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Straight Kurta</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Please check ZIYAA size chart for the garments measurements and order a garment size that is at least 2\" greater than your body measurement for the ease of putting on and taking off the garment.</span></li></ul>";
//                pro.last_update = Long.valueOf(100000000);
//                break;
//            case 7 :
//                pro.id = Long.valueOf(3);
//                pro.status = "READY STOCK";
//                pro.image = "B.jpg";
//                pro.price = 25.00;
//                pro.name = "Buttergel";
//                pro.categories = new ArrayList<>();
//                pro.stock = Long.valueOf(150);
//                pro.product_images = new ArrayList<>();
//                pro.price_discount = 0.00;
////                pro.description = "<ul class=\"a-unordered-list a-vertical a-spacing-none\" style=\"box-sizing: border-box; margin: 0px 0px 0px 18px; color: rgb(148, 148, 148); padding: 0px; font-family: &quot;Amazon Ember&quot;, Arial, sans-serif; font-size: 13px;\"><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Care Instructions: First Wash Dry Clean Only. Do Not Spin In Order To Prevent Garment From Becoming Mishappen.</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Fabric : Crepe</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Chinese Collar 3/4Th Sleeve Solid Straight Kurta</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Available Size:S,M,L,XL,XXL,XXXL</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Straight Kurta</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Please check ZIYAA size chart for the garments measurements and order a garment size that is at least 2\" greater than your body measurement for the ease of putting on and taking off the garment.</span></li></ul>";
//                pro.last_update = Long.valueOf(100000000);
//                break;
//            case 8 :
//                pro.id = Long.valueOf(8);
//                pro.status = "READY STOCK";
//                pro.image = "tata-ace-front-disc-brake-pads-1312687.jpg";
//                pro.price = 10.00;
//                pro.name = "Atlas chooty";
//                pro.categories = new ArrayList<>();
//                pro.stock = Long.valueOf(150);
//                pro.product_images = new ArrayList<>();
//                pro.price_discount = 0.00;
////                pro.description = "<ul class=\"a-unordered-list a-vertical a-spacing-none\" style=\"box-sizing: border-box; margin: 0px 0px 0px 18px; color: rgb(148, 148, 148); padding: 0px; font-family: &quot;Amazon Ember&quot;, Arial, sans-serif; font-size: 13px;\"><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Care Instructions: First Wash Dry Clean Only. Do Not Spin In Order To Prevent Garment From Becoming Mishappen.</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Fabric : Crepe</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Chinese Collar 3/4Th Sleeve Solid Straight Kurta</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Available Size:S,M,L,XL,XXL,XXXL</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Straight Kurta</span></li><li style=\"box-sizing: border-box; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"box-sizing: border-box; color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Please check ZIYAA size chart for the garments measurements and order a garment size that is at least 2\" greater than your body measurement for the ease of putting on and taking off the garment.</span></li></ul>";
//                pro.last_update = Long.valueOf(100000000);
//                break;
//            default :  pro = new Product();
//        }
//        return pro;
//    }
    public List<Product> getProducts(int categoryId) {

        List<ProductDTO> products = p;


        List<Product> list = new ArrayList<>();

        for (ProductDTO product : products) {
            if (product.getCategoryId() == categoryId) {
                Product ap = new Product();
                ap.setId((long) product.getId());
                ap.setName(product.getName());
                ap.setImage(product.getImagePath());
                ap.setPrice(product.getRetailPrice());
                ap.setStock((long) product.getBufferStockQty());
                ap.setPrice_discount(product.getDefaultDiscountPct());
                ap.setDescription(product.getDescription());
                list.add(ap);
            }


        }
        return list;
    }

    public Product getProducts(Long productid) {
        Product ap = new Product();

        for (ProductDTO product : p) {
            if (product.getId() == productid) {

                ap.setId((long) product.getId());
                ap.setName(product.getName());
                ap.setImage(product.getImagePath());
                ap.setPrice(product.getRetailPrice());
                ap.setStock((long) product.getBufferStockQty());
                ap.setPrice_discount(product.getDefaultDiscountPct());
                ap.setDescription(product.getDescription());
                ap.setBrang(product.getBrand());
                ap.setCode(product.getCode());
                ap.setCategory(product.getCategory());

            }

        }
        return ap;
    }
}