package com.app.markeet.model;
import java.io.Serializable;

/**
 * Created by DayadraL on 27/12/2015.
 */
public class InvoiceLineSerialDTO implements Serializable {
    public int Id;
    String Serial;
    String ReturnResoan;
    InvoiceLineDTO InvoiceLine;

    public InvoiceLineSerialDTO(){

    }

    public String getReturnResoan() {
        return ReturnResoan;
    }

    public void setReturnResoan(String returnResoan) {
        ReturnResoan = returnResoan;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getSerial() {
        return Serial;
    }

    public void setSerial(String serial) {
        Serial = serial;
    }

    public InvoiceLineDTO getInvoiceLine() {
        return InvoiceLine;
    }

    public void setInvoiceLine(InvoiceLineDTO invoiceLine) {
        InvoiceLine = invoiceLine;
    }


}
