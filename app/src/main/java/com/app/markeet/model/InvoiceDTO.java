package com.app.markeet.model;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class InvoiceDTO implements Serializable {

    public int Id;
    String Type;
    String SubType;
    String Date;
    String SerialPrefix;
    String SerialNo;
    int SerialNoInt;
    int CustomerId;
    double Total;
    String StartTime;
    String DriverName;
    String VehicleName;
    String HelperName;
    String EndTime;
    double Longitude;
    float Accuracy;
    long Time;
    double Latitude;

    int HPInvoiceId;

    String FirstInstallmentDate;

    double InstallmentAmount;

    int InstallmentCount;

    String InterestMethod;

    double AnnualInterestRate;

    int SettlementTermId;

    String DeliveryDate;

    String CustomerName;

    public String getDeliveryDate() {
        return DeliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        DeliveryDate = deliveryDate;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getHelperName() {
        return HelperName;
    }

    public void setHelperName(String helperName) {
        HelperName = helperName;
    }

    public String getDriverName() {
        return DriverName;
    }

    public void setDriverName(String driverName) {
        DriverName = driverName;
    }

    public String getVehicleName() {
        return VehicleName;
    }

    public void setVehicleName(String vehicleName) {
        VehicleName = vehicleName;
    }

    public float getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(float accuracy) {
        Accuracy = accuracy;
    }

    public long getTime() {
        return Time;
    }

    public void setTime(long time) {
        Time = time;
    }

    int BatLevel;

    boolean IsGPSOn;

    String RouteId;

    String AppVersion;

    String Method;

    String PaymentDetails;

    String Remark;

    String OrderType;


    String Approved;


    String CustomerNIC;

    String imageBytes;

    Collection<InvoiceLineDTO> Lines;
    Collection<InvoiceLineDTO> FreeLines;
    Collection<InvoiceDiscountLineDTO> DiscountLines;

    int Guarantee1Id;


    int Guarantee2Id;


    int PrintCount;

    String Notes;

    public InvoiceDTO() {

    }

    public double getAnnualInterestRate() {
        return AnnualInterestRate;
    }

    public void setAnnualInterestRate(double annualInterestRate) {
        AnnualInterestRate = annualInterestRate;
    }


    public String getInterestMethod() {
        return InterestMethod;
    }

    public void setInterestMethod(String interestMethod) {
        InterestMethod = interestMethod;
    }


    public int getInstallmentCount() {
        return InstallmentCount;
    }

    public void setInstallmentCount(int installmentCount) {
        InstallmentCount = installmentCount;
    }

    public double getInstallmentAmount() {
        return InstallmentAmount;
    }

    public void setInstallmentAmount(double installmentAmount) {
        InstallmentAmount = installmentAmount;
    }

    public String getFirstInstallmentDate() {
        return FirstInstallmentDate;
    }

    public void setFirstInstallmentDate(String firstInstallmentDate) {
        FirstInstallmentDate = firstInstallmentDate;
    }

    public String getRouteId() {
        return RouteId;
    }

    public void setRouteId(String routeId) {
        RouteId = routeId;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }


    public int getHPInvoiceId() {
        return HPInvoiceId;
    }

    public void setHPInvoiceId(int hpInvoiceId) {
        HPInvoiceId = hpInvoiceId;
    }

    public int getSettlementTermId() {
        return SettlementTermId;
    }

    public void setSettlementTermId(int settlementTermId) {
        SettlementTermId = settlementTermId;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getCustomerNIC() {
        return CustomerNIC;
    }

    public void setCustomerNIC(String customerNIC) {
        CustomerNIC = customerNIC;
    }

    public String getPaymentDetails() {
        return PaymentDetails;
    }

    public void setPaymentDetails(String paymentDetails) {
        PaymentDetails = paymentDetails;
    }

    public String getSerialPrefix() {
        return SerialPrefix;
    }

    public void setSerialPrefix(String serialPrefix) {
        SerialPrefix = serialPrefix;
    }

    public void setSerialNo(String serialNo) {
        SerialNo = serialNo;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int customerId) {
        CustomerId = customerId;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public double getTotal() {
        return Total;
    }

    public void setTotal(double total) {
        Total = total;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public int getBatLevel() {
        return BatLevel;
    }

    public void setBatLevel(int batLevel) {
        BatLevel = batLevel;
    }

    public boolean isGPSOn() {
        return IsGPSOn;
    }

    public void setGPSOn(boolean isGPSOn) {
        IsGPSOn = isGPSOn;
    }

    public Collection<InvoiceLineDTO> getLines() {
        return Lines;
    }

    public void setLines(Collection<InvoiceLineDTO> lines) {
        Lines = lines;
    }

    public Collection<InvoiceDiscountLineDTO> getDiscountLines() {
        return DiscountLines;
    }

    public void setDiscountLines(Collection<InvoiceDiscountLineDTO> lines) {
        DiscountLines = lines;
    }

    public String getMethod() {
        return Method;
    }

    public void setMethod(String method) {
        Method = method;
    }

    public String getSubType() {
        return SubType;
    }

    public void setSubType(String subType) {
        SubType = subType;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }

    public int getSerialNoInt() {
        return SerialNoInt;
    }

    public void setSerialNoInt(int serialNoInt) {
        SerialNoInt = serialNoInt;
    }

    public String getOrderType() {
        return OrderType;
    }

    public void setOrderType(String orderType) {
        OrderType = orderType;
    }

    public String getApproved() {
        return Approved;
    }

    public void setApproved(String approved) {
        Approved = approved;
    }

    public String getAppVersion() {
        return AppVersion;
    }

    public void setAppVersion(String appVersion) {
        AppVersion = appVersion;
    }

    public Collection<InvoiceLineDTO> getFreeLines() {
        return FreeLines;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setFreeLines(Collection<InvoiceLineDTO> freeLines) {
        FreeLines = freeLines;
    }

    public String getImageBytes() {
        return imageBytes;
    }

    public void setImageBytes(String imageBytes) {
        this.imageBytes = imageBytes;
    }

    public int getGuarantee1Id() {
        return Guarantee1Id;
    }

    public void setGuarantee1Id(int guarantee1Id) {
        Guarantee1Id = guarantee1Id;
    }

    public int getGuarantee2Id() {
        return Guarantee2Id;
    }

    public void setGuarantee2Id(int guarantee2Id) {
        Guarantee2Id = guarantee2Id;
    }


    public int getPrintCount() {
        return PrintCount;
    }

    public void setPrintCount(int printCount) {
        PrintCount = printCount;
    }




    //Functions

    public boolean containsSerial(String serial) {
        boolean res = false;
        if (getLines() != null) {
            //invoice lines
            for (InvoiceLineDTO l : getLines()) {
                if (l.getSerials() != null) {
                    for (InvoiceLineSerialDTO s : l.getSerials()) {
                        if (s.getSerial().compareTo(serial) == 0) {
                            res = true;
                            break;
                        }
                    }
                }
                if (res) {
                    break;
                }
            }

            //free lines
            if (getFreeLines() != null) {
                for (InvoiceLineDTO l : getFreeLines()) {
                    if (l.getSerials() != null) {
                        for (InvoiceLineSerialDTO s : l.getSerials()) {
                            if (s.getSerial().compareTo(serial) == 0) {
                                res = true;
                                break;
                            }
                        }
                    }
                    if (res) {
                        break;
                    }
                }
            }
        }
        return res;
    }

    public void updateFreeIssues(Context context) {
/*
        DAO<ProductDTO> pdao = DAO.initRepo(context,ProductDTO.class);
        //refresh free Lines
        if(getFreeLines()!=null && getFreeLines().size()>0){
            for(InvoiceLineDTO l : getFreeLines()){
                l.setQty(0);
            }
        }

        //update free issues
        if(getLines()!=null && getLines().size()>0){
            for(InvoiceLineDTO l : getLines()){
                FreeIssueSchemeDTO fs = FreeIssueManager.getInstance(context).getFreeIssueForItem(l.getProductId(),l.getQty());
                if(fs!=null) {
                    ProductDTO freeProduct = (ProductDTO) pdao.getById(fs.getFreeProductId());
                    if (fs.isApplyMultiples()) {
                        int qty=0;
                        if(l.getQty()==fs.getMinQty()){
                            qty = fs.getFreeQty();
                        }else{
                            qty = (l.getQty() / fs.getMinQty()) * fs.getFreeQty();
                        }

                        freeProduct.setQty(qty);
                    } else {
                        freeProduct.setQty(fs.getFreeQty());
                    }
                    addFreeIssue(freeProduct);
                }

            }
        }

        //remove qty 0 lines
        if(getFreeLines()!=null){
            List<InvoiceLineDTO> temp = new ArrayList<InvoiceLineDTO>();
            for(InvoiceLineDTO l : getFreeLines()){
                if(l.getQty()<=0){
                    temp.add(l);
                }
            }

            for(InvoiceLineDTO l : temp){
                getFreeLines().remove(l);
            }
        }*/
    }

    public void setTotal() {
        double tot = 0;
        if (getLines() != null) {
            for (InvoiceLineDTO l : getLines()) {
                tot += l.getRate() * l.getQty();
            }
        }
        setTotal(tot);
    }

    public double getFreeValue() {
        double tot = 0;
        if (getFreeLines() != null) {
            for (InvoiceLineDTO l : getFreeLines()) {
                tot += l.getRate() * l.getQty();
            }
        }
        return tot;
    }

    public int getScannedQty(int prodid) {
        int x = 0;
        if (getFreeLines() != null) {
            for (InvoiceLineDTO l : getFreeLines()) {
                if (l.getProductId() == prodid) {
                    if (l.getSerials() != null) {
                        x = l.getSerials().size();
                        break;
                    }
                }
            }
        }
        return x;
    }

    public InvoiceDTO (Checkout global) {

        Lines = new ArrayList<>();
        DiscountLines = new ArrayList<>();

            for (ProductOrderDetail model : global.product_order_detail) {

                InvoiceLineDTO line = new InvoiceLineDTO();
                line.setProductId(model.product_id);
                line.setQty(model.amount);
                line.setFQty(0);
                line.setRQty(0);
                line.setRepQty(0);
                line.setRate(model.price_item);
                line.setDiscPct(0);

                Lines.add(line);
            }

        Random random = new Random();
        //DAO.txn.beginTransaction();
        if (true) {
            int nextId =  random.nextInt();;
            setId(Math.round(nextId * 100000));


            //Date
            Calendar c = Calendar.getInstance();
            SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
            String sysdate = fmt.format(c.getTime());
            setDate(sysdate);
            Log.d("invoice date", sysdate);

            //Customer Id
            setCustomerId(100);

            Log.d("invoice Cust id", String.valueOf(getCustomerId()));
            setTotal(global.product_order.total_order);

            setType("SALE_ORDER");
            setSubType("NORMAL");

            Calendar cc = Calendar.getInstance();
            SimpleDateFormat fmtd = new SimpleDateFormat("HH:mm:ss");
            String time = fmtd.format(cc.getTime());

            //Start Time
            setStartTime(time);

            setEndTime(time);

            //BatLevel

            //int lvl = new BateryManager(contex.inte, this);
            setBatLevel(50);

            //DeliveryDate
          //  setDeliveryDate(global.getDeliveryDate());
            //Location

//            setLatitude(Globles.latitude);
//            setAccuracy(Globles.accuracy);
//            setDriverName(Globles.driverName);
//            setVehicleName(Globles.vehicleName);
//            setHelperName(Globles.helperName);
//            setNotes(global.getNotes());
//            setTime(Globles.time);
//            setLongitude(Globles.longitude);
//            setGPSOn(global.getGPSOn());
            //Approved
 //           setApproved(global.getApprovedType());
  //          setOrderType(global.getInvoiceSubType());


            //App Version
            setAppVersion("5.0");

            //Save to DB
            int stockFactor = 0;
            for (InvoiceLineDTO line : Lines) {
                line.setInvoice(this);
            }

        }

    }
}
