package com.app.markeet.model;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Category implements Serializable {


    public int Id;

    public String Code;

    public String Name;

    public String ImagePath;

    public String getColour() {
        return Colour;
    }

    public void setColour(String colour) {
        Colour = colour;
    }

    public String Colour;


    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Category() {

    }


}
