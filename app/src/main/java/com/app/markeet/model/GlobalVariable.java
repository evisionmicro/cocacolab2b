package com.app.markeet.model;

import android.app.Application;
import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.markeet.data.AppConfig;
import com.app.markeet.data.ProductDTO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GlobalVariable extends Application {


    public static void loadProducts(final Context context) {
        final List<Cart> items = new ArrayList<>();
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = AppConfig.protocol + AppConfig.ipAddress + ":" + AppConfig.port + "/Pdaservice/GetB2BProductList?imei=" +
                AppConfig.deviceId;

        queue.add(new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Gson gson = new Gson();
                        Type listType = new TypeToken<List<ProductDTO>>() {}.getType();
                        List<ProductDTO> productDTOList = gson.fromJson(response, listType);

                        if (productDTOList != null) {
                            for (ProductDTO productDTO : productDTOList) {
                                Product.p.add(productDTO);
                            }

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ));



    }

}
