package com.app.markeet.model;

import java.util.Collection;

/**
 * Created by DayadraL on 05/01/2016.
 */
public class UploadListDTO {

    Collection<InvoiceDTO> Invoices;
    public String DeviceId;

    public Collection<InvoiceDTO> getInvoices() {
        return Invoices;
    }

    public void setInvoices(Collection<InvoiceDTO> invoices) {
        Invoices = invoices;
    }

    public String getDeviceId() {
        return DeviceId;
    }

    public void setDeviceId(String deviceId) {
        DeviceId = deviceId;
    }
}
