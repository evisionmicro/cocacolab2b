package com.app.markeet.model;

import java.io.Serializable;
import java.util.Collection;

/**
 * Created by DayadraL on 27/12/2015.
 */
public class InvoiceDiscountLineDTO implements Serializable {

    public int Id;
    public double DiscountPct;
    public int DiscountSchemeId;
    InvoiceDTO Invoice;

    public InvoiceDiscountLineDTO(){

    }

    public double getDiscountPct() {
        return DiscountPct;
    }

    public void setDiscountPct(double discountPct) {
        DiscountPct = discountPct;
    }

    public int getDiscountSchemeId() {
        return DiscountSchemeId;
    }

    public void setDiscountSchemeId(int discountSchemeId) {
        DiscountSchemeId = discountSchemeId;
    }

    public InvoiceDTO getInvoice() {
        return Invoice;
    }

    public void setInvoice(InvoiceDTO invoice) {
        Invoice = invoice;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }
}
