package com.app.markeet.model;


import java.io.Serializable;
import java.util.Collection;

/**
 * Created by DayadraL on 27/12/2015.
 */
public class InvoiceLineDTO implements Serializable {

    public int Id;
    public Long ProductId;
    int Qty;
    int FQty;
    int RQty;
    int RepQty;
    double Rate;
    double DiscPct;
    String ReturnReason;
    String ConsumptionJournalReason;
    Collection<InvoiceLineSerialDTO> Serials;
    InvoiceDTO Invoice;

    public InvoiceLineDTO(){

    }

    public String getReturnReason() {
        return ReturnReason;
    }

    public void setReturnReason(String returnReason) {
        ReturnReason = returnReason;
    }

    public String getConsumptionJournalReason() {
        return ConsumptionJournalReason;
    }

    public void setConsumptionJournalReason(String consumptionJournalReason) {
        ConsumptionJournalReason = consumptionJournalReason;
    }

    public int getFQty() {
        return FQty;
    }

    public void setFQty(int FQty) {
        this.FQty = FQty;
    }

    public int getRQty() {
        return RQty;
    }

    public void setRQty(int RQty) {
        this.RQty = RQty;
    }

    public int getRepQty() {
        return RepQty;
    }

    public void setRepQty(int repQty) {
        RepQty = repQty;
    }

    public Long getProductId() {
        return ProductId;
    }

    public void setProductId(Long productId) {
        ProductId = productId;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int qty) {
        Qty = qty;
    }

    public double getRate() {
        return Rate;
    }

    public void setRate(double rate) {
        Rate = rate;
    }

    public double getDiscPct() {
        return DiscPct;
    }

    public void setDiscPct(double discPct) {
        DiscPct = discPct;
    }

    public Collection<InvoiceLineSerialDTO> getSerials() {
        return Serials;
    }

    public void setSerials(Collection<InvoiceLineSerialDTO> serials) {
        Serials = serials;
    }


    public InvoiceDTO getInvoice() {
        return Invoice;
    }

    public void setInvoice(InvoiceDTO invoice) {
        Invoice = invoice;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }
}
