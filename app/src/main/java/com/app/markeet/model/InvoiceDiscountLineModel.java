package com.app.markeet.model;

import java.io.Serializable;

public class InvoiceDiscountLineModel implements Serializable{
    int DiscountSchemeId;
    int Priority;
    double DiscountPct;
    double OrginalDiscountPct;
    double DiscountAmount;

    public int getDiscountSchemeId() {
        return DiscountSchemeId;
    }

    public void setDiscountSchemeId(int discountSchemeId) {
        DiscountSchemeId = discountSchemeId;
    }

    public int getPriority() {
        return Priority;
    }

    public void setPriority(int priority) {
        Priority = priority;
    }

    public double getDiscountPct() {
        return DiscountPct;
    }

    public void setDiscountPct(double discountPct) {
        DiscountPct = discountPct;
    }

    public double getDiscountAmount() {
        return DiscountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        discountAmount = DiscountAmount;
    }

    public double getOrginalDiscountPct() {
        return OrginalDiscountPct;
    }

    public void setOrginalDiscountPct(double orginaldiscountPct) {
        OrginalDiscountPct = orginaldiscountPct;
    }

    public InvoiceDiscountLineModel(int discountSchemeId, double discountPct, double discountAmount, int priority) {
        this.DiscountSchemeId = discountSchemeId;
        this.DiscountPct = discountPct;
        this.DiscountAmount = discountAmount;
        this.OrginalDiscountPct = discountPct;
        this.Priority = priority;
    }

}
