package com.app.markeet.model;

import java.io.Serializable;

/**
 * Created by DayadraL on 25/12/2015.
 */
public class OutstandingInvoiceDTO implements Serializable {

    public int Id;
    String Date;
    int CustomerId;
    String Type;
    String CustomerName;
    String InvoiceSerialNo;
    String Status;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    double Amount;
    double PDAPaidAmount;
    double Outstanding;

    boolean IsSelected;
    boolean ShowCheckBox;

    public boolean isShowCheckBox() {
        return ShowCheckBox;
    }

    public void setShowCheckBox(boolean showCheckBox) {
        ShowCheckBox = showCheckBox;
    }

    public boolean isSelected() {
        return IsSelected;
    }

    public void setSelected(boolean selected) {
        IsSelected = selected;
    }

    double Payment;

    public OutstandingInvoiceDTO() {

    }


    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public int getInvoiceId() {
        return Id;
    }

    public void setInvoiceId(int invoiceId) {
        Id = invoiceId;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int customerId) {
        CustomerId = customerId;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getInvoiceSerialNo() {
        return InvoiceSerialNo;
    }

    public void setInvoiceSerialNo(String invoiceSerialNo) {
        InvoiceSerialNo = invoiceSerialNo;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }

    public double getPDAPaidAmount() {
        return PDAPaidAmount;
    }

    public void setPDAPaidAmount(double pdaPaidAmount) {
        PDAPaidAmount = pdaPaidAmount;
    }


    public double getPayment() {
        return Payment;
    }

    public void setPayment(double payment) {
        Payment = payment;
    }


    public double getOutstanding() {
        return Outstanding;
    }

    public void setOutstanding(double outstanding) {
        Outstanding = outstanding;
    }
}
