package com.app.markeet.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.markeet.ActivityCategoryDetails;
import com.app.markeet.ActivityMain;
import com.app.markeet.ActivityShoppingCart;
import com.app.markeet.R;
import com.app.markeet.adapter.AdapterCategory;
import com.app.markeet.connection.API;
import com.app.markeet.connection.RestAdapter;
import com.app.markeet.connection.callbacks.CallbackCategory;
import com.app.markeet.data.AppConfig;
import com.app.markeet.model.Category;
import com.app.markeet.utils.NetworkCheck;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentCategory extends Fragment {

    private View root_view;
    private RecyclerView recyclerView;
    private Call<CallbackCategory> callbackCall;
    private AdapterCategory adapter;
    private RequestQueue queue;
    public  List<Category> itemscate = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_category, null);
        queue = Volley.newRequestQueue(getActivity());
        queue.add(downloadCategories());
        requestListCategory();

        return root_view;
    }


    private StringRequest downloadCategories() {

        final String url = AppConfig.protocol + AppConfig.ipAddress + ":" + AppConfig.port + "/Pdaservice/GetB2BProductCategories?imei=" + AppConfig.deviceId;
        StringRequest strRequest = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            List<Category> list = new ArrayList<Category>();

                            if (!response.equals("")) {
                                JSONArray arr = new JSONArray(response);
                                for (int i = 0; i < arr.length(); i++) {
                                    Gson gson = new GsonBuilder().create();
                                    Category category = gson.fromJson(arr.getJSONObject(i).toString(), Category.class);
                                    list.add(category);
                                    itemscate.add(category);
                                }
                                recyclerView = (RecyclerView) root_view.findViewById(R.id.recyclerView);
                                recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));

                                adapter = new AdapterCategory(getActivity(), itemscate);

                                recyclerView.setAdapter(adapter);
                                recyclerView.setNestedScrollingEnabled(true);


                                adapter.setOnItemClickListener(new AdapterCategory.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(View view, Category obj) {
                                        Snackbar.make(root_view, obj.getName(), Snackbar.LENGTH_SHORT).show();

                                        Intent i = new Intent(getContext(), ActivityShoppingCart.class);
                                        i.putExtra("CategoryId",obj.getId());
                                        startActivity(i);
//                                        ActivityCategoryDetails.navigate(getActivity(), obj);

                                    }
                                });

                            }

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d("Error", error.toString());
            }
        });

        strRequest.setRetryPolicy(new DefaultRetryPolicy(60000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        return strRequest;
    }


//    private void initComponent() {
//        recyclerView = (RecyclerView) root_view.findViewById(R.id.recyclerView);
//        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
//
//        List<Category> categories = new ArrayList<Category>();
//
//        Category category1 = new Category();
//        category1.icon = "https://www.atlas.lk/myshop/wp-content/uploads/2020/11/Books-800x800-min.jpg";
//        category1.name="Color products";
//        categories.add(category1);
//
//        Category category2 = new Category();
//        category2.icon = "https://www.atlas.lk/myshop/wp-content/uploads/2020/11/Pens-800x800-min.jpg";
//        category2.name = "Pens";
//        categories.add(category2);
//
//        Category category3 = new Category();
//        category3.icon = "https://www.atlas.lk/myshop/wp-content/uploads/2020/11/School-Products-800x800-min.jpg";
//        category3.name = "School";
//        categories.add(category3);
//
//        Category category4 = new Category();
//        category4.icon = "https://www.atlas.lk/myshop/wp-content/uploads/2020/11/Office-Products-800x800-min.jpg";
//        category4.name = "Office";
//        categories.add(category4);
//
//        Category category5 = new Category();
//        category5.icon = "https://www.atlas.lk/myshop/wp-content/uploads/2020/11/Bags-800x800-min.jpg";
//        category5.name = "Bags";
//        categories.add(category5);
//
//        //set data and list adapter
//        adapter = new AdapterCategory(getActivity(), categories);
//        recyclerView.setAdapter(adapter);
//        recyclerView.setNestedScrollingEnabled(false);

//        adapter.setOnItemClickListener(new AdapterCategory.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, Category obj) {
//                Snackbar.make(root_view, obj.name, Snackbar.LENGTH_SHORT).show();
//                ActivityCategoryDetails.navigate(getActivity(), new );
//            }
//        });
//    }


    private void requestListCategory() {
//        API api = RestAdapter.createAPI();
//        callbackCall = api.getListCategory();
//        callbackCall.enqueue(new Callback<CallbackCategory>() {
//            @Override
//            public void onResponse(Call<CallbackCategory> call, Response<CallbackCategory> response) {
//                CallbackCategory resp = response.body();
//                if (resp != null && resp.status.equals("success")) {
//                    recyclerView.setVisibility(View.VISIBLE);
//                    adapter.setItems(new Category().getCategories());
//                    //adapter.setItems(resp.categories);
//                    ActivityMain.getInstance().category_load = true;
//                    ActivityMain.getInstance().showDataLoaded();
//                } else {
//                    onFailRequest();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CallbackCategory> call, Throwable t) {
//                Log.e("onFailure", t.getMessage());
//                if (!call.isCanceled()) onFailRequest();
//            }
//
//        });
    }

    private void onFailRequest() {
        if (NetworkCheck.isConnect(getActivity())) {
            showFailedView(R.string.msg_failed_load_data);
        } else {
            showFailedView(R.string.no_internet_text);
        }
    }

    private void showFailedView(@StringRes int message) {
        ActivityMain.getInstance().showDialogFailed(message);
    }

}
