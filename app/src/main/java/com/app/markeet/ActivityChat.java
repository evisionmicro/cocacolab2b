package com.app.markeet;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActivityChat extends AppCompatActivity {

    private Button chatButton;
    private EditText chatText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_chat);

        chatButton = findViewById(R.id.chat_btn);
        chatText = findViewById(R.id.ChatRoomMessage);

        chatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chatText.setText("");
            }
        });
    }
}