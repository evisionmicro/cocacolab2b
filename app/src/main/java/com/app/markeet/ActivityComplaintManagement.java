package com.app.markeet;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class ActivityComplaintManagement extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Button complaintButton;

    String[] complaintTypes = {"Damaged Products", "", ""};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_complaint_management);

        complaintButton = findViewById(R.id.complaintButton);
        complaintButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Spinner complaintSpinner = (Spinner) findViewById(R.id.complainSpinner);
        complaintSpinner.setOnItemSelectedListener(this);

        ArrayAdapter complaintTypesA = new ArrayAdapter(this,android.R.layout.simple_spinner_item,complaintTypes);
        complaintTypesA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        complaintSpinner.setAdapter(complaintTypesA);

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

}